import React from "react";
import "./City.css";
import { DayForecast } from "../App";


export interface CityProps {
  name: string;
  celsius: number;
  sky: string;
  wind: number;
  sunrise: string;
  sunset: string;
  forecasts: DayForecast[];
}
const City: React.FC<CityProps> = ({
  name,
  celsius,
  sky,
  wind,
  sunrise,
  sunset,
  forecasts,
}) => {
  return (
    <div>
      <h2>{name}</h2>
      <p>Teplota: {celsius}°C</p>
      <p>Obloha: {sky}</p>
      <p>Vietor: {wind} km/h</p>
      <p>Východ slnka: {sunrise}</p>
      <p>Západ slnka: {sunset}</p>
      <h3>Predpoveď: </h3>
      <ul>
        {forecasts.map((f) => (
          <li key={name + f.date} className="forecasts">
            Dátum: {f.date}  min : {f.minTemp}, max : {f.maxTemp}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default City;
