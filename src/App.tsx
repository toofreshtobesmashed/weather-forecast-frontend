import "./App.css";
import City from "./components/City";
import { useEffect, useState } from "react";
import { BASE_URL } from "./config";

export type CityModel = {
  city: string;
  temperature: number;
  sky: string;
  time: String;
  wind: number;
  iconUrl: string;
  sunrise: string;
  sunset: string;
  forecasts: DayForecast[];
};
export type DayForecast = {
  date: string;
  minTemp: string;
  maxTemp: string;
};
export type WeatherModel = {
  cities: CityModel[];
};

const App = () => {
  const [cities, setCities] = useState<WeatherModel | undefined>();

  async function reloadData() {
    const res = await fetch(`${BASE_URL}/tokyo/liptovsky%20mikulas/bratislava`);
    const data = await res.json();
    console.log(data);
    setCities(data);
  }

  useEffect(() => {
    reloadData();
  }, []);

  return (
    <section>
      <button onClick={reloadData}>REFRESH</button>
      <div className="container">
        {cities?.cities.map((city) => (
          <City
            name={city.city}
            celsius={city.temperature}
            sky={city.sky}
            wind={city.wind}
            sunrise={city.sunrise}
            sunset={city.sunset}
            forecasts={city.forecasts}
          />
        ))}
      </div>
    </section>
  );
};

export default App;